# Buildstream issue

To reproduce this bug, go into the `main/` directory and try to run `bst build hello.bst`. You'll get an error about `freedesktop-sdk.bst` not being tracked
(even though it is, in the `bootstrap/` directory). If you try to follow the instructions it gives you (`bst track freedesktop-sdk.bst`), you'll get an error
that `freedestkop-sdk.bst` doesn't exist. If you try to `bst track bootstrap.bst:freedesktop-sdk.bst` followed by `bst build hello.bst`, it'll complain that
`hello.bst` and `sh.bst` in the bootstrap project aren't tracked, even though they are in the `bootstrap/` directory. Finally, if you manually track those
two elements from the `main/` project (`bst track bootstrap.bst:sh.bst bootstrap.bst:hello.bst`), followed by a `bst build hello.bst`, it'll work
